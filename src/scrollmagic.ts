import { TimelineLite, TimelineMax, TweenLite, TweenMax, Linear, Ease } from 'gsap';
import * as ScrollMagicLib from 'scrollmagic';
import 'imports-loader?define=>false!scrollmagic/scrollmagic/minified/plugins/debug.addIndicators.min.js'; // For debugging indicators
import 'imports-loader?define=>false!scrollmagic/scrollmagic/minified/plugins/animation.velocity.min.js';
import 'imports-loader?define=>false!scrollmagic/scrollmagic/minified/plugins/animation.gsap.min.js';
import { isUndefined } from 'util';

export class ScrollMagic {
    private _controller: any;
    private _scenes: Scene[] = [];

    /**
     * Permits to construct a controller that controls animated scrolling scenes
     * @param container Selector or DOM object that will contains the scroll animation controller
     * @param isVertical If the animation are based on vertical scrolling or horizontal scrolling
     */
    public constructor (container: string | object = window, isVertical: boolean = true){
        this._controller = new ScrollMagicLib.Controller({
            container: container,
            vertical: isVertical
        });
    }

    /**
     * Permits to add scenes to the controller
     * @param scenes Scene to add to the controller
     */
    public AddScenes(scenes: Scene | Scene[]){
        if (!Array.isArray(scenes)){
            scenes = [scenes];
        }
        for(var i = 0; i < (scenes as Scene[]).length; i++){
            const scene = scenes[i];
            scene.AddToController(this._controller);
            this._scenes.push(scene);
        }
    }

    /**
     * Destroy the controller and all scene indicators
     */
    public Destroy(){
        try {
            this._controller.destroy();
        } catch (error) {
            console.warn("Error on scrollmagic destroy: " + error);      
            // At least remove indicators
            for (var i = 0; i < this._scenes.length; i++){
                this._scenes[i].RemoveIndicators();
            }      
        }
    }
}

export class Scene {

    /** If indicators are enabled or not */
    public static IndicatorsEnabled: boolean = true;

    private _scene: any;

    /**
     * Build a Scene
     * @param duration Scene scrolling duration
     * @param offset Offset for the trigger position
     * @param triggerEl Selector or DOM object for the trigger element
     * @param triggerHook Position of the trigger hook in relation to the viewport (between 0 and 1). "onEnter" = 1, "onCenter" = 0.5, "onLeave" = 0
     * @param reverse If the scene is reversed when scrolling up
     */
    constructor (
        duration: number | string | Function = 0,
        offset: number = 0,
        triggerEl: string | object = null,
        triggerHook: number | string = "onCenter",
        reverse: boolean = true
    ){
        this._scene = new ScrollMagicLib.Scene({
            duration: duration,
            offset: offset,
            triggerElement: triggerEl,
            triggerHook: triggerHook,
            reverse: reverse
        });
    }

    //#region Basic Methods 
    /**
     * Do not use this function, use ScrollMagic.AddScenes instead 
     * @param ctrl Scroll Magic Controller object
     */
    public AddToController(ctrl: any){
        this._scene.addTo(ctrl);
    }
    /**
     * Permits to add indicators for debugging
     * @param name Name of indicator (default: undefined)
     */
    public AddIndicators(name: string = undefined) : Scene{
        if (Scene.IndicatorsEnabled){
            if (isUndefined(name)){
                this._scene.addIndicators();
            }
            else{
                this._scene.addIndicators({name: name});
            }
        }

        return this;
    }
    /**
     * Permits to remove indicators
     */
    public RemoveIndicators(){
        this._scene.removeIndicators();
    }
    //#endregion Basic Methods

    //#region Tween Usage
    /**
     * Permits to set a GSAP built tween object on the scene
     * @param tween A GSAP Tween object
     */
    public setTween(tween: TimelineLite | TimelineMax | TweenLite | TweenMax): Scene {
        this._scene.setTween(tween);

        return this;
    }
    /**
     * Permits to set a basic tween without using GSAP object
     * @param targetEl Selector of DOM Object
     * @param animation The tween parameters (animation settings)
     * @param duration Duration of the tween
     */
    public setTween_Simple(
        targetEl: string | object, 
        animation: any, 
        duration: number | object = 1
    ) : Scene {
        this._scene.setTween(targetEl, duration, animation);

        return this;
    }

    /**
     * Permits to set a basic from to animation without using GSAP object
     * @param target Selector or DOM object that represents the target
     * @param duration Duration in second
     * @param from From object configuration
     * @param to To object configuration
     * @param ease The animation ease
     * @param repeat Number of times that animation should repeat after its first iteration
     * @param yoyo If true, every other repeat cycle will run in the opposite direction 
     */
    public setTween_FromTo(
        target: string | object,
        duration: number, 
        from: any,
        to: any,
        ease: Ease = Linear.easeNone,
        repeat: number = 0, 
        yoyo: boolean = false
    ) : Scene {
        to.ease = ease;
        to.yoyo = yoyo;
        to.repeat = repeat;

        this._scene = this._scene.setTween(
            TweenMax.fromTo(target, duration, from, to)
        );

        return this;
    }

    /**
     * Permits to set a basic to animation without using GSAP object
     * @param target Selector or DOM object that represents the target
     * @param duration Duration in second
     * @param to To object configuration
     * @param ease The animation ease
     * @param repeat Number of times that animation should repeat after its first iteration
     * @param yoyo If true, every other repeat cycle will run in the opposite direction 
     */
    public setTween_To(
        target: string | object,
        duration: number, 
        to: any,
        ease: Ease = Linear.easeNone,
        repeat: number = 0, 
        yoyo: boolean = false
    ) : Scene {
        to.ease = ease;
        to.yoyo = yoyo;
        to.repeat = repeat;

        this._scene = this._scene.setTween(
            TweenMax.to(target, duration, to)
        );

        return this;
    }
    /**
     * Permits to set a basic from stagger from to animation without using GSAP object
     * @param target Selector or DOM object that represents the target
     * @param duration Duration in second
     * @param from From object configuration
     * @param to To object configuration
     * @param stagger Amount of time in seconds to stagger the start time of each tween (default: 0) 
     * @param ease The animation ease
     * @param repeat Number of times that animation should repeat after its first iteration
     * @param yoyo If true, every other repeat cycle will run in the opposite direction 
     */
    public setTween_staggerFromTo(
        targets: string | any[],
        duration: number,
        from: any,
        to: any,
        stagger: number = 0, 
        ease: Ease = Linear.easeNone,
        repeat: number = 0, 
        yoyo: boolean = false
    ) : Scene {
        to.ease = ease;
        to.yoyo = yoyo;
        to.repeat = repeat;

        this._scene = this._scene.setTween(
             TweenMax.staggerFromTo(targets, duration, from, to, stagger)
         );   

        return this;
    }
    //#endregion Tween Usage

    //#region Other scroll magic methods
    
    /**
     * Permits to create a Velocity animation
     * @param animatedEls One or more DOM elements or a selector used as target of the animation
     * @param animation The animation object
     * @param duration Duration in ms
     * @param ease Easing
     */
    public setVelocity(
        animatedEls: string | object,
        animation: any,
        duration: number = 0,
        ease: Ease = Linear.easeNone
    ) : Scene {
        
        this._scene.setVelocity(animatedEls, animation, {duration: duration, easing: ease});

        return this;
    }

    /**
     * Permits to toggle class inside an animation
     * @param animatedEl Selector or DOM object that represent the animated element
     * @param classes Classes to toggle during animation ("classA classB ..." for multiple classes)
     */
    public setClassToggle(animatedEl: string | object, classes: string) : Scene {
        this._scene = this._scene.setClassToggle(animatedEl, classes);

        return this;
    }

    /**
     * Add a callback function when entering in the animation
     * @param f The callback (event) => {}
     */
    public onEnter(f: Function) : Scene {
        this._scene.on("enter", f);
        return this;
    }
    /**
     * Add a callback when leaving an animation
     * @param f The callback (event) => {}
     */
    public onLeave(f: Function) : Scene {
        this._scene.on("leave", f);
        return this;
    }
    /**
     * Add a callback when the animation is progressing
     * @param f The callback (event) => {}
     */
    public onProgress(f: Function) : Scene {
        this._scene.on("progress", f);
        return this;
    }

    /**
     * Permits to pin an object during the animation
     * @param animatedEl The element to pin
     * @param spacerClass Class of the spacer created for pinning
     * @param pushFollowers If true following elements will be "pushed" down for the duration of the pin, if false the pinned element will just scroll past them. Ignored, when duration is 0.
     */
    public setPin(
        animatedEl: string | object,
        spacerClass: string = "scrollmagic-pin-spacer",
        pushFollowers: boolean = true
    ) : Scene {
        this._scene = this._scene.setPin(animatedEl, {pushFollowers: pushFollowers, spacerClass: spacerClass});
        return this;
    }

    //#endregion

}

// PATCH FOR ADDING MULTIPLE CLASSES
ScrollMagicLib._util.addClass = function _patchedAddClass(elem, classname) {
    if (classname) {
        if (elem.classList) {
            classname.split(' ').forEach(function _addCls(c) {
            elem.classList.add(c);
            });
        } else elem.className += ' ' + classname;
    }
};
ScrollMagicLib._util.removeClass = function _patchedRemoveClass(elem, classname) {
    if (classname) {
        if (elem.classList) {
            classname.split(' ').forEach(function _removeCls(c) {
            elem.classList.remove(c);
            });
        } else elem.className = elem.className.replace(new RegExp('(^|\b)' + classname.split(' ').join('|') + '(\b|$)', 'gi'), ' ');
    }
};