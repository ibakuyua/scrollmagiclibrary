# Scroll Magic Library #

All is set up for using scroll magic and GSAP easily on Angular projects

## Install

`npm install --save scrollmagiclib`

Add to your *tsconfig.json* the following path:
```json
{
    "compilerOptions": {
        ...
        "paths": {
            "velocity": ["node_modules/velocity-animate/velocity.min.js"]
        },
    }
}
```

## Use

Here are two examples of how use this library but first you have to create a parent div that will contains the *animation controller* ID.

```html
<div id="scrollmagiccontroller" class="animation-container">
  <div>
    YOUR SCENES/ANIMATIONS
  </div>
</div>
```

### Simple Animation
* HTML File
```html
<div class="square" id="animate"></div>
<div id="trigger"></div>
<br><br><br><br><br>
<br><br><br><br><br>
<br><br><br><br><br>
<br><br><br><br><br>
<br><br><br><br><br>
<br><br><br><br><br>
```

* Typescript File
```typescript
import { Component, AfterViewInit, OnDestroy } from '@angular/core';
import { ScrollMagic, Scene } from 'scrollmagiclib';

@Component({
  selector: 'app-simple-animation',
  templateUrl: './simple-animation.component.html',
  styleUrls: ['./simple-animation.component.scss']
})
export class SimpleAnimationComponent implements AfterViewInit, OnDestroy {

  scrollmagic: ScrollMagic;

  constructor () {
  }

  ngAfterViewInit(): void {
    this.scrollmagic = new ScrollMagic("#scrollmagiccontroller");

    var scene1 = new Scene(750, 0, "#trigger");
    scene1.setTween_Simple("#animate", {backgroundColor: "blue", transform: "rotate(90deg)"} );
    scene1.AddIndicators("1 (duration 750)");
    var scene2 = new Scene(0, 0, "#trigger2");
    scene2.setTween_Simple("#animate2", {scale: 0.5}, 0.75);
    scene2.AddIndicators("2 (duration 0)");
    
    this.scrollmagic.AddScenes([scene1, scene2]);
  }

  ngOnDestroy(){
    this.scrollmagic.Destroy();
  }

}
```

* And the result:

![](simpleanimation.gif)

### Using GSAP for SVG Drawing

* HTML File
```html
<div class="draw">
  <svg width= "600px" height = "400px">
    <path id="word" d="
      M22.328,70.018c9.867-7.4,10.724,20.434,13.014,28.694c-0.08-9.105-1.308-31.463,11.936-31.886
      c11.313-0.361,17.046,19.368,16.367,28.098c-1.432-10.289,6.234-30.682,18.163-25.671c11.505,4.833,8.682,26.772,20.071,31.964
      c13.06,5.953,14.854-8.305,19.734-17.017c7.188-12.836,4.933-15.417,29.6-14.8c-8.954-3.842-37.42,1.728-28.539,20.1
      c5.823,12.045,34.911,12.583,30.018-8.873c-5.385,17.174,24.01,23.104,24.01,9.123c0-9.867,3.816-15.937,16.034-18.5
      c8.359-1.754,18.982,4.754,25.9,9.25c-10.361-4.461-51.941-13.776-37.749,12.357c9.435,17.372,50.559,2.289,33.477-6.063
      c-2.871,19.008,32.415,31.684,30.695,54.439c-2.602,34.423-66.934,24.873-79.302,2.134c-13.11-24.101,38.981-36.781,54.798-40.941
      c8.308-2.185,42.133-12.162,25.88-25.587c-2.779,17.058,19.275,28.688,29.963,12.911c6.862-10.131,6.783-25.284,30.833-19.117
      c-9.404-0.429-32.624-0.188-32.864,18.472c-0.231,17.912,21.001,21.405,40.882,11.951
    "></path>
  </svg>
</div>
<br><br><br><br><br><br>
<br><br><br><br><br><br>
<div id="trigger"></div>
<br><br><br><br><br><br>
<br><br><br><br><br><br>
<br><br><br><br><br><br>
```

* Typescript File

``` typescript
import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { ScrollMagic, Scene } from 'scrollmagiclib';
import { TimelineMax } from 'gsap';

@Component({
  selector: 'app-svg-drawing',
  templateUrl: './svg-drawing.component.html',
  styleUrls: ['./svg-drawing.component.scss']
})
export class SvgDrawingComponent implements OnDestroy, AfterViewInit {

  scrollmagic: ScrollMagic;

  constructor() { }

  ngAfterViewInit(){
    this.scrollmagic = new ScrollMagic("#scrollmagiccontroller");

    // Prepare svg
    var wordEl = (document.getElementById("word") as any) as SVGPathElement;
    var lineLength = wordEl.getTotalLength();
    wordEl.style.strokeDasharray = lineLength + "px";
    wordEl.style.strokeDashoffset = lineLength + "px";

    // Create scene
    var scene = new Scene(400, 0, "#trigger", 0);
    var tween = new TimelineMax()
      .to("#word", 1, {strokeDashoffset: 0, ease: "none"})
      .to("path", 1, {stroke: "red", ease: "none"}, 0);
    scene.setTween(tween);
    scene.AddIndicators("Draw");

    this.scrollmagic.AddScenes(scene);
  }
  ngOnDestroy() {
    this.scrollmagic.Destroy();
  }

}
```

* And the result: 
![](svgdrawing.gif)

## Author: Ibakuyumcu Arnaud

The libraries used are:
* Scrollmagic: https://scrollmagic.io (you can see a ton of examples in their website)
* GSAP: https://greensock.com (To know how to use Tween and Timeline manually)
* Velocity: http://velocityjs.org/
